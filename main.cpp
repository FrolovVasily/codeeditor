#include <QApplication>
#include "menubar.h"

#include "mainwindow.h"
#include <QSyntaxHighlighter>
#include <QPlainTextEdit>
#include <QPalette>

class SyntaxHighlighter: public QSyntaxHighlighter
{
    Q_OBJECT

private:
    QStringList m_lstKeywords;

protected:
    enum {NormalState = -1, InsideCStyleComment, InsideCString};

    virtual void highlightBlock(const QString&);

    QString getKeyword(int i, const QString& str) const;

public:
    SyntaxHighlighter(QTextDocument* parent = 0);
};

SyntaxHighlighter::SyntaxHighlighter(QTextDocument* parent)
    : QSyntaxHighlighter(parent)
{
    m_lstKeywords << "int" << "bool" << "for";
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
//    MainWindow w;
//    w.show();

//    MenuBar menu;
//    QObject::connect(&menu, &MenuBar::exit, &app, &QApplication::quit);
//    menu.show();
    QPlainTextEdit txt;
    QFont fnt("Lucida Console", 10, QFont::Normal);
    txt.document()->setDefaultFont(fnt);
    txt.resize(640, 480);
    txt.show();

    new SyntaxHighlighter(txt.document());

    QPalette pal = txt.palette();
    pal.setColor(QPalette::Base, Qt::darkBlue);
    pal.setColor(QPalette::Text, Qt::yellow);
    txt.setPalette(pal);

    return app.exec();
}
