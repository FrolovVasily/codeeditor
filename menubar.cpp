#include "menubar.h"

MenuBar::MenuBar()
{
    CreateFileMenuItem();
    CreateEditMenuItem();
    CreateSettingsMenuItem();
    CreateHelpMenuItem();

    addMenu(pFile);
    addMenu(pEdit);
    addMenu(pSettings);
    addMenu(pHelp);
}

void MenuBar::CreateFileMenuItem()
{
    pFile = new QMenu("&File");
    pFile->setStatusTip(tr("Main actions"));

    // create actions
    pFileNew = new QAction(tr("&New"), this);
    pFileNew->setShortcuts(QKeySequence::New);
    connect(pFileNew, &QAction::triggered, this, &MenuBar::s_FileNew);

    pFileOpen = new QAction(tr("&Open"), this);
    pFileOpen->setShortcuts(QKeySequence::Open);
    connect(pFileOpen, &QAction::triggered, this, &MenuBar::s_FileOpen);

    pFileSave = new QAction(tr("&Save"), this);
    pFileSave->setShortcuts(QKeySequence::Save);
    connect(pFileSave, &QAction::triggered, this, &MenuBar::s_FileSave);

    pFileSaveAs = new QAction(tr("S&ave As..."), this);
    connect(pFileSaveAs, &QAction::triggered, this, &MenuBar::s_FileSaveAs);

    pFileExit = new QAction(tr("&Exit"), this);
    pFileExit->setShortcuts(QKeySequence::Quit);
    connect(pFileExit, &QAction::triggered, this, &MenuBar::s_FileExit);

    // add actions
    pFile->addAction(pFileNew);
    pFile->addAction(pFileOpen);
    pFile->addAction(pFileSave);
    pFile->addAction(pFileSaveAs);
    pFile->addSeparator();
    pFile->addAction(pFileExit);
}

void MenuBar::CreateEditMenuItem()
{
    pEdit = new QMenu("&Edit");
    pEdit->setStatusTip(tr("Edit actions"));

    // create actions
    pEditCut = new QAction(tr("&Cut"), this);
    pEditCut->setShortcuts(QKeySequence::Cut);
    connect(pEditCut, &QAction::triggered, this, &MenuBar::s_EditCut);

    pEditCopy = new QAction(tr("&Copy"), this);
    pEditCopy->setShortcuts(QKeySequence::Copy);
    connect(pEditCopy, &QAction::triggered, this, &MenuBar::s_EditCopy);

    pEditPaste = new QAction(tr("&Paste"), this);
    pEditPaste->setShortcuts(QKeySequence::Paste);
    connect(pEditPaste, &QAction::triggered, this, &MenuBar::s_EditPaste);

    // add actions
    pEdit->addAction(pEditCut);
    pEdit->addAction(pEditCopy);
    pEdit->addAction(pEditPaste);
}

void MenuBar::CreateSettingsMenuItem()
{
    pSettings = new QMenu("&Settings");

    // create actions
    pSettingsSetTheme = new QAction(tr("Set &theme"), this);
    connect(pSettingsSetTheme, &QAction::triggered, this, &MenuBar::s_SetTheme);

    // add actions
    pSettings->addAction(pSettingsSetTheme);
}

void MenuBar::CreateHelpMenuItem()
{
    pHelp = new QMenu("&Help");

    // create actions
    pHelpAbout = new QAction(tr("&About"), this);
    connect(pHelpAbout, &QAction::triggered, this, &MenuBar::s_About);

    // add actions
    pHelp->addAction(pHelpAbout);
}

// File slots
void MenuBar::s_FileNew()
{
    emit newFile();
}

void MenuBar::s_FileOpen()
{
    emit openFile();
}

void MenuBar::s_FileSave()
{
    emit saveFile();
}

void MenuBar::s_FileSaveAs()
{
    emit saveFileAs();
}

void MenuBar::s_FileExit()
{
    emit exit();
}

// Edit slots
void MenuBar::s_EditCut()
{
    emit cut();
}

void MenuBar::s_EditCopy()
{
    emit copy();
}

void MenuBar::s_EditPaste()
{
    emit paste();
}

// Settings slots
void MenuBar::s_SetTheme()
{
    emit setTheme();
}

// Help slots
void MenuBar::s_About()
{
    emit about();
}

