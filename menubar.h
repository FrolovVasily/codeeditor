#ifndef MENUBAR_H
#define MENUBAR_H

#include <QMenuBar>
#include <QAction>
#include <QMenu>

class MenuBar : public QMenuBar
{
    Q_OBJECT
public:
    MenuBar();

signals:
    // File item
    void newFile();
    void openFile();
    void saveFile();
    void saveFileAs();
    void exit();

    // Edit item
    void cut();
    void copy();
    void paste();

    // Settings item
    void setTheme();

    // Help item
    void about();


private slots:
    // File item
    void s_FileNew();
    void s_FileOpen();
    void s_FileSave();
    void s_FileSaveAs();
    void s_FileExit();

    // Edit item
    void s_EditCut();
    void s_EditCopy();
    void s_EditPaste();

    // Settings item
    void s_SetTheme();

    // Help item
    void s_About();

private:
    void CreateFileMenuItem();
    void CreateEditMenuItem();
    void CreateSettingsMenuItem();
    void CreateHelpMenuItem();

private:
    QMenu *pFile;
    QAction *pFileNew;
    QAction *pFileOpen;
    QAction *pFileSave;
    QAction *pFileSaveAs;
    QAction *pFileExit;

    QMenu *pEdit;
    QAction *pEditCut;
    QAction *pEditCopy;
    QAction *pEditPaste;

    QMenu *pSettings;
    QAction *pSettingsSetTheme;

    QMenu *pHelp;
    QAction *pHelpAbout;
};

#endif // MENUBAR_H
